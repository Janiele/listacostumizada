package com.example.jarlaslnovo15.listacustomizada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Main_Nomes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__nomes);


        String[] nomes = {"Rick Grimes", "Daryl Dixon","Carl Grimes","Carol Peletier","Maggie Greene","Michonne","Glenn Rhee",
                "Rosita Espinosa","Sasha Williams","Tara Chambler","Eugene Porter","Gabriel Stokes","Hershel Greene","Andrea","Morgan Jones",
                "Abraham Ford","Tyreese","O Governador","Shane Walsh","Negan","Paul \"Jesus\" Rovia","Merle Dixon","Ezekiel","Noah"};

       ListView lv = (ListView)findViewById(R.id.list_item);

        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, nomes);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Nome clicado: " + i, Toast.LENGTH_SHORT).show();
            }
        });


    }

}
