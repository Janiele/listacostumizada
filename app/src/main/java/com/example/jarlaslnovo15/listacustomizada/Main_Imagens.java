package com.example.jarlaslnovo15.listacustomizada;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Main_Imagens extends AppCompatActivity {

    ListView listView;

    String[] nameArray = {"Rick Grimes", "Daryl Doxin", "Carol Peletier","Maggie Greene","Michonne","Glenn Rhee",
            "Rosita Espinosa","Sasha Williams","Tara Chambler","Eugene Porter","Gabriel Stokes","Hershel Greene",
            "Andrea","Morgan Jones","Abraham Ford","Tyreese","O Governador","Shane Walsh","Negan","Merle Dixon",
            "Ezekiel","Noaha"};

    String[] infoArray = {"Andrew Lincoln", "Norman Reedus","Melissa McBride","Chandler Riggs", "Lauren CohanDanai", "Gurira",
            "Steven Yeun","Christian Serratos","Christian Serratos","Sonequa Martin-Green", "Josh McDermitt", "Seth Gilliam",
            "Scott Wilson","Laurie Holden","Lennie James","Michael Cudlitz","Chad L. Coleman","Jon Bernthal",
            "David Morrissey", "Jeffrey Dean Morgan","Tom Payne", "Michael Rooker", "Khary Payton","Tyler James Williams"};

    Integer[] imageIDarray = {R.drawable.rickgrimes, R.drawable.daryldixon, R.drawable.carol, R.drawable.maggie,
            R.drawable.michonne, R.drawable.glennrhee, R.drawable.rositaespinosa, R.drawable.sasha, R.drawable.tara,
            R.drawable.eugene_porter, R.drawable.padre, R.drawable.ershel, R.drawable.andrea, R.drawable.morgan,
            R.drawable.abraham, R.drawable.tyreese, R.drawable.governador, R.drawable.shane, R.drawable.negan,R.drawable.jesus,
            R.drawable.merle, R.drawable.reiezekiel, R.drawable.noah};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PersonCustomAdapter adapter = new PersonCustomAdapter(this, nameArray, infoArray, imageIDarray);

        listView =(ListView)findViewById(R.id.listviewID);
        listView.setAdapter(adapter);
    }
}
