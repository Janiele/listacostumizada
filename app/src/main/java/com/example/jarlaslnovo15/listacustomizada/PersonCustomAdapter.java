package com.example.jarlaslnovo15.listacustomizada;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by JarlasLNovo15 on 02/04/2018.
 */

public class PersonCustomAdapter extends ArrayAdapter {

    private final Activity context ; // para referenciar o contexto da Activity
    private Integer[] imageIDarray; // para armazenar as imagens dos personagens
    private String[] nameArray; // para armazenar a lista de nomes dos personagens
    private String[] infoArray; // para armazenar a lista de nomes dos atores e atrizes

    public PersonCustomAdapter(Activity context, String[] nameArray, String[] infoArray, Integer[] imageIDArray){

        super(context,R.layout.listview_two, nameArray);
        this.context=context;
        this.imageIDarray = imageIDArray;
        this.nameArray = nameArray;
        this.infoArray = infoArray;


    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listview_two, null, true);

        // este código obtém referências a objetos no arquivo layout_two.xml

        TextView nameTextField = (TextView) rowView.findViewById(R.id.nametextViewID);
        TextView infoTextField = (TextView) rowView.findViewById(R.id.infotextViewID);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageViewID);

        //este código define os valores dos objetos para os valores das matrizes

        nameTextField.setText(nameArray[position]);
        infoTextField.setText(infoArray[position]);
        imageView.setImageResource(imageIDarray[position]);

        return rowView;
    }
}
