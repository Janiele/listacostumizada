package com.example.jarlaslnovo15.listacustomizada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botaoNomes = (Button)findViewById(R.id.botaoNomes);
        Button botaoNumeros = (Button)findViewById(R.id.botaoNumeros);
        Button botaoImagens = (Button)findViewById(R.id.botaoImagens);

         botaoNomes.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i= new Intent(MainActivity.this, Main_Nomes.class);
                 startActivity(i);
             }
         });

         botaoNumeros.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i= new Intent(MainActivity.this, Main_Numeros.class);
                 startActivity(i);

             }
         });
         botaoImagens.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i= new Intent(MainActivity.this, Main_Imagens.class);
                 startActivity(i);

             }
         });


    }
}
