package com.example.jarlaslnovo15.listacustomizada;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Main_Numeros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__numeros);
        //numeros de episodios feitos
        Integer[] numeros = {94,88,76,76,73,69,66,49,44,44,42,33,32,31,31,29,22,19,19,18,16,14,13,10 };
        final ListView listView = (ListView)findViewById(R.id.list_item);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, numeros);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Quantidade de episódios feitos por: " + i, Toast.LENGTH_SHORT).show();
            }
        });


    }
}
